public class Pelicula {

private String nombre;
private String director;
private String categoria;
private int duracion;

public Pelicula () {
 
 nombre="El terror de la programacion en JAVA";
 director="Roberto Escobar Aguero";
 categoria="Suspenso";
 duracion=18;
 
 }//fin metodo constructor sin parametros
 
public Pelicula (String nombre, String director, String categoria, int duracion){
 
 this.nombre=nombre;
 this.director=director;
 this.categoria=categoria;
 this.duracion=duracion;
 
 }//fin metodo constructor con parametros
 
public void setNombre(String nombre){
 
 this.nombre=nombre;
 
 }//fin metodo setCapacidad
 
public String getNombre(){
 
 return nombre;
 }//fin metodo getCapacidad
 
public void setDirector(String director){
 
 this.director=director;
 
 }//fin metodo setCapacidad
 
public String getDirector(){
 
 return director;
 }//fin metodo getCapacidad
 
public void setCategoria(String categoria){
 
 this.categoria=categoria;
 
 }//fin metodo setCapacidad
 
public String getCategoria(){
 
 return categoria;
 }//fin metodo getCapacidad
 
public void setDuracion(int duracion){
 
 this.duracion=duracion;
 
 }//fin metodo setCapacidad
 
public int getDuracion(){
 
 return duracion;
 }//fin metodo getCapacidad
 
public String toString(){
 return "Nombre de la pelicula: "+nombre+"\n"+
 "Duracion: "+duracion+" semanas"+ "\n"+
 "Director: "+director+"\n"+
 "Categoria: "+categoria;
 }//fin metodo toString
}//fin clase
